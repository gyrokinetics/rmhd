program rmhd

  use mhd_data, only: input, nprint, nwrite, dt, time, nstep
  use fields, only: timo, diss
  use fields, only: z, v, w, dz, dv, d_diss
  use nlps, only: nlpsc
  use grid, only: rkperp2, rkperp2inv, malias, nalias
  use constants, only: debug

  use devObject, only: hadamard, devVar, open_devObjects, close_devObjects, transfer_c4
  use devObject, only: use_gpu, transfer_r4, devf_zeros, c2g, g2c
  use mhd_device_func
  
  implicit none

  complex, dimension(:,:,:), allocatable :: vtmp, ztmp, ntmp, nl

  type (devVar), dimension(2) :: odv, odz, dntmp, dnl
  type (devVar) :: dk2inv, dk2

  real :: hdt
  integer :: ncycle
  logical :: exit = .false.

!
! Initialize GPU for computing
  call open_devObjects
  
!
! Read namelists and set defaults
  call input
  if (debug) write (*,*) 'finished input'

! Set up arrays, constants, and ic's
  call init
  if (debug) write (*,*) 'finished init'

! Allocate local arrays
  call allocate_local 
  if (debug) write (*,*) 'finished allocate_local'

  if (use_gpu) then
     call transfer_r4 (rkperp2(:,:,1),    dk2,    c2g)
     call transfer_r4 (rkperp2inv(:,:,1), dk2inv, c2g)
  end if
  time = 0. 

!
! ****************************************************
!                     Top of the loop
! ****************************************************
  do ncycle = 0, nstep-1
     if (exit) exit

     time = time + dt
     hdt = 0.5*dt

!
! Solve Eqs(23) of Schekochihin et al.
! zeta+, zeta- denoted by z(:,:,1), z(:,:,2)
! In other words: 
! zeta+ = z(ky, kx, 1)
! zeta- = z(ky, kx, 2)
! Similarly:
! v = -kperp**2 z(ky, kx, +/-)
! where v == "vorticity" in some generalized sense.
!

!
! The code advances the nonlinear terms in time using RK4
!
     if (use_gpu) then
!        z = 0.
        call d_nonlinear (dv, dz, dntmp)
        call dev_mhd_prop(odv, odz, dv, dntmp, dk2inv, hdt) 

!        call transfer_c4 (vtmp(:,:,1), odv(1), g2c)
!        write (*,*) vtmp(1:4,1:4,1)
!        stop 

        call d_nonlinear (odv, odz, dnl)
        call dev_mhd_prop(odv, odz, dv, dnl, dk2inv, hdt)
        call dev_mhd_prop2 (dntmp, dnl, 2.)

        call d_nonlinear (odv, odz, dnl)
        call dev_mhd_prop(odv, odz, dv, dnl, dk2inv, dt)
        call dev_mhd_prop2 (dntmp, dnl, 2.)

        call d_nonlinear (odv, odz, dnl)
        call dev_mhd_prop3 (dv, dz, dntmp, dnl, d_diss, dk2inv, dt/6.)

     else
        call f_nonlinear (v, z, ntmp)     
        call mhd_prop (vtmp, ztmp, v, ntmp, rkperp2inv, hdt)

        call f_nonlinear (vtmp, ztmp, nl)
        call mhd_prop (vtmp, ztmp, v, nl, rkperp2inv, hdt)
        call mhd_prop2 (ntmp, nl, 2.)

        call f_nonlinear (vtmp, ztmp, nl)
        call mhd_prop (vtmp, ztmp, v, nl, rkperp2inv, dt)
        call mhd_prop2 (ntmp, nl, 2.)

        call f_nonlinear (vtmp, ztmp, nl)
        call mhd_prop3 (v, z, ntmp, nl, diss, rkperp2inv, dt/6.)

     end if

!
! Save some results for later
!
     if(mod(ncycle,nwrite) == nwrite-1) then
        nprint = nprint + 1
        if (use_gpu) then
           call transfer_c4 (z(:,:,1), dz(1), g2c)
           call transfer_c4 (z(:,:,2), dz(2), g2c)
        end if
        w(nprint,1) = sum(rkperp2(:,:,1)*(conjg(z(:,:,1))*z(:,:,1)))
        w(nprint,2) = sum(rkperp2(:,:,2)*(conjg(z(:,:,2))*z(:,:,2)))

        timo(nprint)=time
        write (*,*) time, w(nprint,1), w(nprint,2)

        open (unit=55, file="stop", status="old", err=100)
        exit = .true.
100     continue
     endif

  end do
!
! ****************************************************
!                     Bottom of the loop
! ****************************************************

  if (use_gpu) then
     call transfer_c4 (z(:,:,1), dz(1), g2c)
     call transfer_c4 (z(:,:,2), dz(2), g2c)
     call transfer_c4 (v(:,:,1), dv(1), g2c)
     call transfer_c4 (v(:,:,2), dv(2), g2c)
  end if


!  call output
  
contains

  subroutine mhd_prop (vtmp, ztmp, v, ntmp, rkperp2inv, hdt)
    complex, dimension (:,:,:) :: vtmp, ztmp, v, ntmp
    real, dimension (:,:,:) :: rkperp2inv
    real :: hdt
    
    vtmp = v - hdt * ntmp
    ztmp = - vtmp * rkperp2inv     
    
  end subroutine mhd_prop

  subroutine mhd_prop2 (ntmp, nl, s)
    complex, dimension (:,:,:) :: ntmp, nl
    real :: s

    ntmp = ntmp + s*nl

  end subroutine mhd_prop2
  
  subroutine mhd_prop3 (v, z, ntmp, nl, diss, rkperp2inv, sdt)
    complex, dimension (:,:,:) :: v, z, ntmp, nl
    real, dimension (:,:,:) :: diss, rkperp2inv
    real :: sdt
    
    v = v - sdt * (ntmp + nl)
    ! Dissipation is only first-order accurate...problem?
    v = v * diss
    ! Find zeta at the new time step
    z = - v * rkperp2inv
    
  end subroutine mhd_prop3

  subroutine f_nonlinear (v, z, nl)

    complex, dimension (:,:,:), intent (in) :: v, z
    complex, dimension (:,:,:), intent (out) :: nl

    complex, dimension (:,:,:), allocatable, save :: nonlin

    logical :: first = .true.

    if (first) then
       allocate (nonlin(malias, nalias, 3)) ;  nonlin = 0.
       first = .false.
    end if
    
    call nlpsc(0, z(:,:,1),  v(:,:,2), nonlin(:,:,1))
    call nlpsc(1, z(:,:,1),  z(:,:,2), nonlin(:,:,3))
    call nlpsc(2, v(:,:,1),  z(:,:,2), nonlin(:,:,2))

    nl(:,:,1) = 0.5*(nonlin(:,:,1) - nonlin(:,:,2) + rkperp2(:,:,1) * nonlin(:,:,3))
    nl(:,:,2) = 0.5*(nonlin(:,:,1) - nonlin(:,:,2) - rkperp2(:,:,1) * nonlin(:,:,3))

!    write (*,*) nl(1:4,1:4,1)
!    stop 

  end subroutine f_nonlinear

  subroutine d_nonlinear (dv, dz, dnl)

    type(devVar), dimension(2) :: dv, dz, dnl
    type (devVar), dimension(3),save ::dnonlin

    logical :: first = .true.

    if (first) then
       dnonlin(1) = alloc_dv('complex',malias,nalias) ; call devf_zeros (dnonlin(1))
       dnonlin(2) = alloc_dv('complex',malias,nalias) ; call devf_zeros (dnonlin(2))
       dnonlin(3) = alloc_dv('complex',malias,nalias) ; call devf_zeros (dnonlin(3))
       first = .false.
    end if

!    call transfer_c4(nl(:,:,1), dnonlin(1), g2c)
!    write (*,*) nl(1:4,1:4,1)
!    stop 

    
    call nlpsc(0, dz(1), dv(2), dnonlin(1))
    call nlpsc(1, dz(1), dz(2), dnonlin(3))
    call nlpsc(2, dv(1), dz(2), dnonlin(2))   
        
    call dev_mhd_nl (dnl, dnonlin, dk2)


  end subroutine d_nonlinear

  subroutine init
  !     
  !     Set up arrays and define values of constants.
    !
    use nlps, only: init_nlps
    use fields, only: alloc_fields
    use grid, only: init_grid
    use mhd_data, only: iexp, rdiff_0
    use devObject, only: devVar, transfer_c4, c2g

! Initialize grid quantities
    call init_grid
    if (debug) write (*,*) 'finished init_grid'
    
! Allocate main 3-D arrays and some diagnostic arrays
    call alloc_fields (nstep/nwrite+1)
    if (debug) write (*,*) 'finished alloc_fields'

! Initialize fft's for nonlinear terms
    call init_nlps
    if (debug) write (*,*) 'finished init_nlps'

! Initialize the main fields
    call pertb  
    if (debug) write (*,*) 'finished pertb'

! Initialize the dissipative terms
    diss = 1./(1.+dt*rdiff_0*rkperp2**iexp)    
    if (use_gpu) then
       call transfer_r4 (diss(:,:,1), d_diss, c2g)
    end if

  end subroutine init

  subroutine allocate_local
    
    use grid, only: malias, nalias
    use devObject, only: alloc_dv, devf_zeros, devVar, transfer_c4

    if (use_gpu) then
       odv(1)    = alloc_dv('complex',malias,nalias) ; call devf_zeros(odv(1))
       odv(2)    = alloc_dv('complex',malias,nalias) ; call devf_zeros(odv(2))

       odz(1)    = alloc_dv('complex',malias,nalias) ; call devf_zeros(odz(1))
       odz(2)    = alloc_dv('complex',malias,nalias) ; call devf_zeros(odz(2))

       dnl(1)    = alloc_dv('complex',malias,nalias) ; call devf_zeros(dnl(1))
       dnl(2)    = alloc_dv('complex',malias,nalias) ; call devf_zeros(dnl(2))

       dntmp(1)  = alloc_dv('complex',malias,nalias) ; call devf_zeros(dntmp(1))
       dntmp(2)  = alloc_dv('complex',malias,nalias) ; call devf_zeros(dntmp(2))

       dk2       = alloc_dv('real',malias,nalias)    ; call devf_zeros(dk2)
       dk2inv    = alloc_dv('real',malias,nalias)    ; call devf_zeros(dk2inv)
    else
       if (allocated(vtmp)) deallocate (vtmp)  ; allocate (vtmp(malias, nalias, 2)) ; vtmp = 0.
       if (allocated(ztmp)) deallocate (ztmp)  ; allocate (ztmp(malias, nalias, 2)) ; ztmp = 0.
       if (allocated(ntmp)) deallocate (ntmp)  ; allocate (ntmp(malias, nalias, 2)) ; ntmp = 0.
       if (allocated(nl))   deallocate (nl)    ; allocate (nl  (malias, nalias, 2)) ; nl = 0.
    end if


  end subroutine allocate_local

  subroutine pertb
!
! This subroutine inserts the perturbation.
!
    use constants
    use fields
    use grid, only: rkperp2, malias, nalias
    use nlps, only: f_reality

    z  = 0.  

    z(2, 1, 1) = 4.     ! zeta_plus (ky=1, kx=0) = 4 
    z(1, 2, 1) = 2.     ! zeta_plus (ky=0, kx=1) = 2
    z(1, 3, 1) = 1.     ! zeta_plus (ky=0, kx=2) = 1

    
    z(1, 2, 2) =  2.    ! zeta_minus (ky=0, kx=1) =  2
    z(1, 3, 2) = -1.    ! zeta_minus (ky=0, kx=2) = -1

!    z = 0.
!    z(2,2,1) = 1.
!    z(1,2,2) = 1.

!           
! Enforce the reality condition for the ky=0 components:
!
    
    if (debug) write (*,*) 'About to call reality'
    call f_reality (z(:,:,1))
    call f_reality (z(:,:,2))

    v = - rkperp2 * z

    if (use_gpu) then
       call transfer_c4 (v(:,:,1), dv(1), c2g)
       call transfer_c4 (v(:,:,2), dv(2), c2g)
       call transfer_c4 (z(:,:,1), dz(1), c2g)
       call transfer_c4 (z(:,:,2), dz(2), c2g)
    end if

!    if (debug) then
!       do m=1,malias
!          do n=1,nalias
!             write (*,100) m,n,z(m,n,1),z(m,n,2)
!          end do
!          write (*,*) 
!       end do
!    end if

100 format (2(1x,i3),4(1x,2es10.3))

  end subroutine pertb

  subroutine output

    use mhd_data, only: nprint
    use fields, only: timo
    use grid, only: malias, nalias, rkx, rky

    complex, dimension(:,:), allocatable :: tmp
    integer i

    open(unit=31,file='output',form='formatted')

!NEED TO TRANSFER DATA BACK TO CPU FOR OUTPUTTING


    do i=1,nprint
       write (31,*) timo(i), w(i,1), w(i,2)
    end do
        
    close(31)

    allocate (tmp(malias, nalias))

    tmp = 0.5*(z(:,:,1)+z(:,:,2))
    tmp = conjg(tmp)*tmp
    call subk ('phi2.dat', tmp)

    tmp = 0.5*(z(:,:,1)-z(:,:,2))
    tmp = conjg(tmp)*tmp
    call subk ('psi2.dat', tmp)

    tmp = 0.5*(z(:,:,1)+z(:,:,2))
    call sub ('pot.dat', tmp)

    tmp = -rkperp2(:,:,1)*0.5*(z(:,:,1)+z(:,:,2))
    call sub ('vort.dat', tmp)

    tmp = 0.5*(z(:,:,1) - z(:,:,2))
    call sub('psi.dat', tmp)

    tmp = -rkperp2(:,:,1)*0.5*(z(:,:,1) - z(:,:,2))
    call sub('jpar.dat', tmp)

    tmp = z(:,:,1)
    call sub ('z_plus.dat',  tmp)

    tmp = z(:,:,2)
    call sub ('z_minus.dat', tmp)

    tmp = v(:,:,1)
    call sub ('v_plus.dat',  tmp)

    tmp = v(:,:,2)
    call sub ('v_minus.dat', tmp)

  end subroutine output
    
  subroutine sub (filename, tmp)
    
    use constants
    use grid, only: x, y, x0, y0
    character(*), intent(in) :: filename
    complex, dimension (:,:) :: tmp
    integer :: m, n
    
    open(unit=32,file=trim(filename),form='formatted')
    
    call FFT2_C2C_D (tmp,tmp,malias,nalias,1)
    
    do n=1,nalias
       do m=1,malias
          write (32,*) y(m), x(n), real(tmp(m,n))
       end do
       write (32,*) y(1)+2.*pi*y0, x(n), real(tmp(1,n))
       write (32,*) 
    end do
    do m=1,malias
       write (32,*) y(m), x(1)+2.*pi*x0, real(tmp(m,1))
    end do
    write (32,*) y(1)+2.*pi*y0, x(1)+2.*pi*x0, real(tmp(1,1))
    write (32,*)
    
    close(32)
    
  end subroutine sub

  subroutine subk (filename, tmp)
    
    use constants
    use grid, only: rkx, rky
    use nlps, only: scalefac
    character(*), intent(in) :: filename
    complex, dimension (:,:) :: tmp
    integer :: m, n
    
    open(unit=32,file=trim(filename),form='formatted')

    do n=1,nalias
       do m=1,malias
          if (scalefac(m,n) > 0.) &
               write (32,100) rky(m), rkx(n), real(tmp(m,n)), aimag(tmp(m,n))
       end do
       write (32,*) 
    end do
        
    close(32)

100  format (4(4x,2es10.2))

  end subroutine subk

end program rmhd

