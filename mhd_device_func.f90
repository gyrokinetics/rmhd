!____Nail's module for customized device functions used in itg
!
!            all functions used in itg are implemented as function of class 
!
!            devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal,cmplxhandling),   
!
!            which asumes dense storage: matrix sizes coinside with leading dimensions   
!

module mhd_device_func

  use devObject

  implicit none

contains

  !-------------------------------------------------------------------------------------------    

  subroutine dev_mhd_prop(ov,oz,v,n,k2i,h)

    !         puts group of operators in RK propagation on the GPU

    !           ov1 = v1 - h*n1
    !           ov2 = v2 - h*n2
    !           oz1 = -ov1*k2inv
    !	        oz2 = -ov2*k2inv

    implicit none

    type(devVar), dimension(2) :: ov,oz,v,n
    type(devVar) :: k2i
    real h
    type(devVar), dimension(9) :: devVars
    integer(4) fPtr,ndevvars/9/,nparint/0/,nparreal/1/
    integer(4) parint(1)
    real(4) parreal(1)

    devVars(1)=ov(1)
    devVars(2)=ov(2)
    devVars(3)=oz(1)
    devVars(4)=oz(2)
    devVars(5)=v(1)
    devVars(6)=v(2)
    devVars(7)=n(1)
    devVars(8)=n(2)
    devVars(9)=k2i 
    parreal(1)=-h   ! note negative sign

    call cf_getdevfuncptr('custom_mhd_prop',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine dev_mhd_prop

  subroutine dev_mhd_prop2 (ntmp, nl, s)
    
    ! For each component: 
    !
    !           ntmp = ntmp + s * nl
    !

    implicit none

    type(devVar), dimension(2) :: ntmp, nl
    real s
    type(devVar), dimension(4) :: devVars
    integer(4) fPtr,ndevvars/4/,nparint/0/,nparreal/1/
    integer(4) parint(1)
    real(4) parreal(1)

    devVars(1)=ntmp(1)
    devVars(2)=ntmp(2)
    devVars(3)=nl(1)
    devVars(4)=nl(2)
    parreal(1)=s

    call cf_getdevfuncptr('custom_mhd_prop2',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine dev_mhd_prop2

  subroutine dev_mhd_prop3 (v, z, ntmp, nl, diss, k2i, sdt)
    
    ! For each component: 
    !
    !           v = v - sdt * (ntmp + nl)
    !           v = v * diss
    !           z = - v * k2i

    implicit none

    type(devVar), dimension(2) :: v, z, ntmp, nl
    type(devVar) :: diss, k2i
    real :: sdt
    type(devVar), dimension(10) :: devVars
    integer(4) fPtr,ndevvars/10/,nparint/0/,nparreal/1/
    integer(4) parint(1)
    real(4) parreal(1)

    devVars(1) = v(1)
    devVars(2) = v(2)
    devVars(3) = z(1)
    devVars(4) = z(2)
    devVars(5) = ntmp(1)
    devVars(6) = ntmp(2)
    devVars(7) = nl(1)
    devVars(8) = nl(2)
    devVars(9) = diss
    devVars(10)= k2i 
    parreal(1) = -sdt ! note negative sign


    call cf_getdevfuncptr('custom_mhd_prop3',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine dev_mhd_prop3

  subroutine dev_mhd_nl (nl, nonlin, dk2)
    
    !
    !  nl1 = 0.5*(nonlin1 - nonlin2 + dk2 * nonlin3)
    !  nl2 = 0.5*(nonlin1 - nonlin2 - dk2 * nonlin3)
    !           
    !           

    implicit none

    type(devVar), dimension(2) :: nl
    type(devVar), dimension(3) :: nonlin
    type(devVar) :: dk2
    type(devVar), dimension(6) :: devVars
    integer(4) fPtr,ndevvars/6/,nparint/0/,nparreal/1/
    integer(4) parint(1)
    real(4) parreal(1)

    devVars(1) = nl(1)
    devVars(2) = nl(2)
    devVars(3) = nonlin(1)
    devVars(4) = nonlin(2) 
    devVars(5) = nonlin(3)
    devVars(6) = dk2
    parreal(1) = 0.5


    call cf_getdevfuncptr('custom_mhd_nl',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine dev_mhd_nl

  !-------------------------------------------------------------------------------------------    

  subroutine devcust_itg_hadam2(a,ikx,kxa2,iky,kya2)  

    !           puts the following operation on the GPU
    !           also can be realized (maybe a bit slower) by calling twice intrinsic devObject function dev_hadamardf with option 'i'
    !           
    !               kxa2 = ikx*a           ! call devf_hadamardf(kxa2,a,ikx,'i')
    !               kya2 = iky*a           ! call devf_hadamardf(kya2,a,iky,'i')
    !                       
    !           ikx and iky are purely imaginary stored as real
    !           a,kxa2,kya2 are complex
    !             

    implicit none

    type(devVar) a,ikx,kxa2,iky,kya2
    type(devVar) devVars(5)
    integer(4) fPtr,ndevvars/5/,nparint/0/,nparreal/0/
    integer(4) parint(2)
    real(4) parreal(1)

    devVars(1)=a
    devVars(2)=ikx
    devVars(3)=kxa2
    devVars(4)=iky
    devVars(5)=kya2

    call cf_getdevfuncptr('custom_itg_hadam2',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine devcust_itg_hadam2

  !-------------------------------------------------------------------------------------------    

  subroutine devcust_itg_det2 (nl_term,kxa2,kxb2,kya2,kyb2)   
    !       
    !           puts the following operation (pointwise 2x2 determinant) on the GPU 
    !           
    !               nl_term = kxa2 * kyb2 - kxb2 * kya2

    !            nl_term,kxa2,kxb2,kya2,kyb2 are complex

    implicit none

    type(devVar) nl_term,kxa2,kxb2,kya2,kyb2
    type(devVar) devVars(5)
    integer(4) fPtr,ndevvars/5/,nparint/0/,nparreal/0/
    integer(4) parint(1)
    real(4) parreal(1)

    devVars(1)=nl_term
    devVars(2)=kxa2
    devVars(3)=kxb2
    devVars(4)=kya2
    devVars(5)=kyb2

    call cf_getdevfuncptr('custom_itg_det2',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine devcust_itg_det2

  !-------------------------------------------------------------------------------------------    

end module mhd_device_func
