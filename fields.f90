module fields

  use devObject
  implicit none

  complex, dimension(:,:,:), allocatable :: z,  v
  real, dimension(:), allocatable :: timo
  real, dimension(:,:), allocatable :: w
  real, dimension(:,:,:), allocatable :: diss

  type (devVar), dimension(2) :: dv, dz
  type (devVar) :: d_diss

contains

  subroutine alloc_fields (n)
    
    use grid, only: malias, nalias
    integer, intent (in) :: n

    if(allocated(v)) deallocate(v)  ; allocate (v(malias, nalias, 2))  ; v = 0.
    if(allocated(z)) deallocate(z)  ; allocate (z(malias, nalias, 2))  ; z = 0.
    if(allocated(W)) deallocate(W)  ; allocate (W(n, 2))               ; W = 0.

    if(allocated(timo)) deallocate(timo) ; allocate (timo(n))               ; timo = 0.
    if(allocated(diss)) deallocate(diss) ; allocate (diss(malias,nalias, 2)); diss = 0.
    
    if (use_gpu) then
       dv(1) = alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv(1))    
       dv(2) = alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv(2))    

       dz(1) = alloc_dv('complex',malias,nalias)  ; call devf_zeros(dz(1))    
       dz(2) = alloc_dv('complex',malias,nalias)  ; call devf_zeros(dz(2))    

       d_diss = alloc_dv('real',malias,nalias)  ; call devf_zeros(d_diss)    
    end if

  end subroutine alloc_fields

end module fields

