module fft_work
  
  real(8), allocatable :: work1_fftd(:), work2_fftd(:)
  real(4), allocatable :: work1_ffts(:), work2_ffts(:)
  
end module fft_work
