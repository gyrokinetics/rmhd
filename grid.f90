module grid
!
! This module contains routines and variables related to the computational grid.
!
  implicit none

  integer nffty	     !  # of y grid points 
  integer nfftx	     !  # of x grid points
  integer md	     ! total # of ky modes after dealiasing
  integer nd	     ! total # of kx modes after dealiasing
  integer :: nddp, malias, nalias, madd, nadd, mddp

  real :: x0, y0

  real, dimension(:,:,:), allocatable :: rkperp2    ! kperp**2 for each mode
  real, dimension(:,:,:), allocatable :: rkperp2inv ! 1/kperp**2 for each mode
  real, dimension(:), allocatable :: rkx, rky
  real, dimension(:), allocatable :: x, y
  integer, dimension(:), allocatable :: mrr, nrr

contains

  subroutine init_grid 

    use constants, only: pi
    integer :: m, n

    malias=nffty
    md=2*((nffty-1)/3)+1
    mddp=md/2+1
    madd=malias-md
    
    nalias=nfftx
    nd=2*((nfftx-1)/3)+1
    nddp=nd/2+1
    nadd=nalias-nd
    
    call alloc_grid

    do n=1,nddp
       nrr(n)=n-1
    enddo
    do n=nddp+nadd+1,nalias
       nrr(n)=n-nalias-1
    end do
    
       
    do m=1,mddp
       mrr(m)=m-1
    enddo
    do m=mddp+madd+1,malias
       mrr(m)=m-malias-1
    end do
    
    rky = real(mrr)/y0
    rkx = real(nrr)/x0

    do n=1,nddp
       do m=1,mddp
          rkperp2(m,n,1) = rkx(n)**2 + rky(m)**2
          rkperp2(m,n,2) = rkx(n)**2 + rky(m)**2
       end do
       do m=mddp+madd+1,malias
          rkperp2(m,n,1) = rkx(n)**2 + rky(m)**2
          rkperp2(m,n,2) = rkx(n)**2 + rky(m)**2
       end do
    end do

    do n=nddp+nadd+1,nalias
       do m=1,mddp
          rkperp2(m,n,1) = rkx(n)**2 + rky(m)**2
          rkperp2(m,n,2) = rkx(n)**2 + rky(m)**2
       end do
       do m=mddp+madd+1,malias
          rkperp2(m,n,1) = rkx(n)**2 + rky(m)**2
          rkperp2(m,n,2) = rkx(n)**2 + rky(m)**2
       end do
    end do

    where (rkperp2 > epsilon(0.)) rkperp2inv = 1./rkperp2

    do m=1,malias
       y(m) = real(m-1)*2.*pi*y0/real(malias)
    end do

    do n=1,nalias
       x(n) = real(n-1)*2.*pi*x0/real(nalias)
    end do

!    do n=1,nalias
!       do m=1,malias
!          write (*,*) rkx(n), rky(m), rkperp2(m,n,1)
!       end do
!       write (*,*) 
!    end do

  end subroutine init_grid
  
  subroutine alloc_grid

    if(allocated(mrr)) deallocate(mrr)         ;     allocate (mrr(malias))   ;  mrr = 0
    if(allocated(nrr)) deallocate(nrr)         ;     allocate (nrr(nalias))   ;  nrr = 0
    if(allocated(rkx)) deallocate(rkx)         ;     allocate (rkx(nalias))   ; rkx = 0.
    if(allocated(rky)) deallocate(rky)         ;     allocate (rky(malias))   ; rky = 0.
    if(allocated(rkperp2)) deallocate(rkperp2) ;     allocate (rkperp2   (malias, nalias, 2)) ; rkperp2 = 0.
    if(allocated(rkperp2inv)) deallocate(rkperp2inv);allocate (rkperp2inv(malias, nalias, 2)) ; rkperp2inv = 0.

    if(allocated(x)) deallocate(x)             ;     allocate (x(nalias)) ; x = 0.
    if(allocated(y)) deallocate(y)             ;     allocate (y(malias)) ; y = 0.

  end subroutine alloc_grid

end module grid
