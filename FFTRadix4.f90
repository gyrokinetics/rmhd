  ! a set of fortran single and double precision routines for fft based on Radix-4 (for sequences of length 2**M, M>=2)
  !=====================================================================================================================
  ! written by Nail Gumerov on 03/19/2007
  ! contains:
  ! FFT1_C2C_D(fftin,fftout,fftlength,forward)
  ! FFT1_C2C_S(fftin,fftout,fftlength,forward)
  ! FFT2_C2C_D(fftin,fftout,fftlength_x,fftlength_y,forward)
  ! FFT2_C2C_S(fftin,fftout,fftlength_x,fftlength_y,forward)
  ! FFT3_C2C_D(fftin,fftout,fftlength_x,fftlength_y,fftlength_z,forward)
  ! FFT3_C2C_S(fftin,fftout,fftlength_x,fftlength_y,fftlength_z,forward)
  !--------------------------------------------------------------------------------------  
    
    subroutine FFT1_C2C_D(fftin,fftout,fftlength,forward)
    ! 1D FFT
    
    use fft_work
 
    complex fftin(*),fftout(*)
    integer fftlength,forward
    
    integer ifault
    integer itype,i
    
    if (forward.eq.0) then
        itype=-1
    else
        itype=1
    endif
    
    allocate (work1_fftd(fftlength))
    allocate (work2_fftd(fftlength))

    do i=1,fftlength
        work1_fftd(i)=real(fftin(i))
        work2_fftd(i)=imag(fftin(i))
    enddo

    call FASTF_D(work1_fftd,work2_fftd,fftlength,itype,ifault)
    if (ifault.ne.0) then
        write(*,*) 'FASTF_D failed due to an error in arguments'
        stop
    endif
    
    do i=1,fftlength
        !write(*,*) work1_fftd(i),work2_fftd(i)
        fftout(i)=cmplx(work1_fftd(i),work2_fftd(i))
        !write(*,*)fftout(i)
    enddo
    
    !stop
    
    deallocate (work1_fftd,work2_fftd)
    
    return
    end
  
  !--------------------------------------------------------------------------------------  
  
    subroutine FFT1_C2C_S(fftin,fftout,fftlength,forward)
    ! 1D FFT
    
    use fft_work
 
    complex(4) fftin(*),fftout(*)
    integer fftlength,forward
    
    integer ifault
    integer itype,i
    
    allocate (work1_ffts(fftlength))
    allocate (work2_ffts(fftlength))

    work1_ffts=real(fftin(1:fftlength))
    work2_ffts=imag(fftin(1:fftlength))
    
    if (forward.eq.0) then
        itype=-1
    else
        itype=1
    endif
    
    call FASTF_S(work1_ffts,work2_ffts,fftlength,itype,ifault)
    if (ifault.ne.0) then
        write(*,*) 'FASTF_S failed due to an error in arguments'
        stop
    endif
    
    do i=1,fftlength
        fftout(i)=cmplx(work1_ffts(i),work2_ffts(i))
    enddo
    
    deallocate (work1_ffts,work2_ffts)
    
    return
    end
 
 !-------------------------------------------------------------------------------------- 
  	
  	subroutine FFT2_C2C_D (fftin,fftout,fftlength_x,fftlength_y,forward)
    ! 2D FFT
    
    use fft_work
    
	implicit none
	complex fftin(*),fftout(*)
	integer fftlength_x,fftlength_y,forward
	integer itype,ifault
	integer j1,j2,inc
	complex cdum

!---------------------------------------

    if (forward.eq.0) then
        itype=-1
    else
        itype=1
    endif

    allocate (work1_fftd(fftlength_y))
    allocate (work2_fftd(fftlength_y))  
	
	do j1=1,fftlength_x	
	    inc=j1    							! y-tranform
		do j2=1,fftlength_y	
		    inc=j1+(j2-1)*fftlength_x	
			cdum=fftin(inc)
			work1_fftd(j2)=real(cdum)
			work2_fftd(j2)=imag(cdum)
			inc=inc+fftlength_x
		enddo
					
		call FASTF_D(work1_fftd,work2_fftd,fftlength_y,itype,ifault)
			
		if(ifault.eq.1) then
			write(*,*)'Error in arguments for FASTF_D: j1 =',j1
			stop
		endif
			
		inc=j1
		do j2=1,fftlength_y
		    inc=j1+(j2-1)*fftlength_x	
			fftout(inc)=cmplx(work1_fftd(j2),work2_fftd(j2))
			inc=inc+fftlength_x
		enddo
	enddo
	
	deallocate(work1_fftd,work2_fftd)
	allocate (work1_fftd(fftlength_x))
    allocate (work2_fftd(fftlength_x))
    
	do j2=0,(fftlength_y-1)*fftlength_x,fftlength_x								! x-tranform
		do j1=1,fftlength_x
			cdum=fftout(j1+j2)
			work1_fftd(j1)=real(cdum)
			work2_fftd(j1)=imag(cdum)
		enddo
			
		call FASTF_D(work1_fftd,work2_fftd,fftlength_x,itype,ifault)
			
		if(ifault.eq.1) then
			write(*,*)'Error in arguments for FASTF_D: j2 =',j2/fftlength_x+1
			stop
		endif
			
		do j1=1,fftlength_x
			fftout(j1+j2)=cmplx(work1_fftd(j1),work2_fftd(j1))
		enddo
	enddo
	
	deallocate(work1_fftd,work2_fftd)				
		
	return
	end

 !-------------------------------------------------------------------------------------- 
	
  	subroutine FFT2_C2C_S (fftin,fftout,fftlength_x,fftlength_y,forward)
    ! 2D FFT
    
    use fft_work
    
	implicit none
	complex fftin(*),fftout(*)
	integer fftlength_x,fftlength_y,fftlength_z,forward
	integer itype,ifault
	integer j1,j2,inc
	complex cdum

!---------------------------------------

    if (forward.eq.0) then
        itype=-1
    else
        itype=1
    endif

    allocate (work1_ffts(fftlength_y))
    allocate (work2_ffts(fftlength_y))  
	
	do j1=1,fftlength_x								! y-tranform
		do j2=1,fftlength_y
			inc=j1
			cdum=fftin(inc)
			work1_ffts(j2)=real(cdum)
			work2_ffts(j2)=imag(cdum)
			inc=inc+fftlength_x
		enddo
					
		call FASTF_D(work1_ffts,work2_ffts,fftlength_y,itype,ifault)
			
		if(ifault.eq.1) then
			write(*,*)'Error in arguments for FASTF_S: j1 =',j1
			stop
		endif
			
		inc=j1
		do j2=1,fftlength_y
			fftout(inc)=cmplx(work1_ffts(j2),work2_ffts(j2))
			inc=inc+fftlength_x
		enddo
	enddo
	
	deallocate(work1_ffts,work2_ffts)
	allocate (work1_ffts(fftlength_x))
    allocate (work2_ffts(fftlength_x))
    
	do j2=0,(fftlength_y-1)*fftlength_x,fftlength_x								! x-tranform
		do j1=1,fftlength_x
			cdum=fftout(j1+j2)
			work1_ffts(j1)=real(cdum)
			work2_ffts(j1)=imag(cdum)
		enddo
			
		call FASTF_S(work1_ffts,work2_ffts,fftlength_x,itype,ifault)
			
		if(ifault.eq.1) then
			write(*,*)'Error in arguments for FASTF_S: j2 =',j2/fftlength_x+1
			stop
		endif
			
		do j1=1,fftlength_x
			fftout(j1+j2)=cmplx(work1_ffts(j1),work2_ffts(j1))
		enddo
	enddo
	
	deallocate(work1_ffts,work2_ffts)				
		
	return
	end
	
!------------------------------------------------------------------------------------ 
  	
  	subroutine FFT3_C2C_D (fftin,fftout,fftlength_x,fftlength_y,fftlength_z,forward)
    ! 3D FFT
    
    use fft_work
    
	implicit none
	complex fftin(*),fftout(*)
	integer fftlength_x,fftlength_y,fftlength_z,forward
	integer isize,itype
	integer ifault
	integer isize2,j1,j2,j3,inc1,inc2
	complex cdum

!---------------------------------------

    if (forward.eq.0) then
        itype=-1
    else
        itype=1
    endif
    
    allocate (work1_fftd(fftlength_z))
    allocate (work2_fftd(fftlength_z))  
	isize2=fftlength_x*fftlength_y
	
	do j1=1,fftlength_x								! z-tranform
		inc1=j1
		do j2=1,fftlength_y
			inc2=inc1
			do j3=1,fftlength_z
				cdum=fftin(inc2)
				work1_fftd(j3)=real(cdum)
				work2_fftd(j3)=imag(cdum)
				inc2=inc2+isize2
			enddo
			
			call FASTF_D(work1_fftd,work2_fftd,fftlength_z,itype,ifault)
			
			if(ifault.eq.1) then
				write(*,*)'Error in arguments for FASTF_D: j1 =',j1,' j2=',j2
				stop
			endif
			
			inc2=inc1
			do j3=1,fftlength_z
				fftout(inc2)=cmplx(work1_fftd(j3),work2_fftd(j3))
				inc2=inc2+isize2
			enddo

			inc1=inc1+fftlength_y
			
		enddo
	enddo
	
	deallocate(work1_fftd,work2_fftd)
	allocate (work1_fftd(fftlength_y))
    allocate (work2_fftd(fftlength_y))
    
	do j1=1,fftlength_x								! y-tranform
		inc1=j1
		do j3=1,fftlength_z
			inc2=inc1
			do j2=1,fftlength_y
				cdum=fftout(inc2)
				work1_fftd(j2)=real(cdum)
				work2_fftd(j2)=imag(cdum)
				inc2=inc2+fftlength_x
			enddo
			
			call FASTF_D(work1_fftd,work2_fftd,fftlength_y,itype,ifault)
			
			if(ifault.eq.1) then
				write(*,*)'Error in arguments for FASTF_D: j1 =',j1,' j3=',j3
				stop
			endif
			
			inc2=inc1
			do j2=1,fftlength_y
				fftout(inc2)=cmplx(work1_fftd(j2),work2_fftd(j2))
				inc2=inc2+fftlength_x
			enddo

			inc1=inc1+isize2
			
		enddo
	enddo
	
	deallocate(work1_fftd,work2_fftd)
	allocate (work1_fftd(fftlength_x))
    allocate (work2_fftd(fftlength_x))
								
	do j2=0,isize2-fftlength_x,fftlength_x					! x-tranform
		inc1=j2
		do j3=1,fftlength_z
			inc2=inc1
			do j1=1,fftlength_x
				inc2=inc2+1
				cdum=fftout(inc2)
				work1_fftd(j1)=real(cdum)
				work2_fftd(j1)=imag(cdum)
			enddo
			
			call FASTF_D(work1_fftd,work2_fftd,fftlength_x,itype,ifault)
			
			if(ifault.eq.1) then
				write(*,*)'Error in arguments for FASTF_D: j2 =',j2/fftlength_x+1,' j3=',j3
				stop
			endif
			
			inc2=inc1
			do j1=1,fftlength_x
				inc2=inc2+1
				fftout(inc2)=cmplx(work1_fftd(j1),work2_fftd(j1))
			enddo

			inc1=inc1+isize2
			
		enddo
	enddo
	
	deallocate(work1_fftd,work2_fftd)						
		
	return
	end
 !-------------------------------------------------------------------------------------- 
  	
  	subroutine FFT3_C2C_S (fftin,fftout,fftlength_x,fftlength_y,fftlength_z,forward)
    ! 3D FFT
    
    use fft_work
    
	implicit none
	complex fftin(*),fftout(*)
	integer fftlength_x,fftlength_y,fftlength_z,forward
	integer isize,itype
	integer ifault
	integer isize2,j1,j2,j3,inc1,inc2
	complex cdum

!---------------------------------------

    if (forward.eq.0) then
        itype=-1
    else
        itype=1
    endif
    
    allocate (work1_ffts(fftlength_z))
    allocate (work2_ffts(fftlength_z))  
	isize2=fftlength_x*fftlength_y
	
	do j1=1,fftlength_x								! z-tranform
		inc1=j1
		do j2=1,fftlength_y
			inc2=inc1
			do j3=1,fftlength_z
				cdum=fftin(inc2)
				work1_ffts(j3)=real(cdum)
				work2_ffts(j3)=imag(cdum)
				inc2=inc2+isize2
			enddo
			
			call FASTF_S(work1_ffts,work2_ffts,fftlength_z,itype,ifault)
			
			if(ifault.eq.1) then
				write(*,*)'Error in arguments for FASTF_S: j1 =',j1,' j2=',j2
				stop
			endif
			
			inc2=inc1
			do j3=1,fftlength_z
				fftout(inc2)=cmplx(work1_ffts(j3),work2_ffts(j3))
				inc2=inc2+isize2
			enddo

			inc1=inc1+fftlength_y
			
		enddo
	enddo
	
	deallocate(work1_ffts,work2_ffts)
	allocate (work1_ffts(fftlength_y))
    allocate (work2_ffts(fftlength_y))
    
	do j1=1,fftlength_x								! y-tranform
		inc1=j1
		do j3=1,fftlength_z
			inc2=inc1
			do j2=1,fftlength_y
				cdum=fftout(inc2)
				work1_ffts(j2)=real(cdum)
				work2_ffts(j2)=imag(cdum)
				inc2=inc2+fftlength_x
			enddo
			
			call FASTF_S(work1_ffts,work2_ffts,fftlength_y,itype,ifault)
			
			if(ifault.eq.1) then
				write(*,*)'Error in arguments for FASTF_S: j1 =',j1,' j3=',j3
				stop
			endif
			
			inc2=inc1
			do j2=1,fftlength_y
				fftout(inc2)=cmplx(work1_ffts(j2),work2_ffts(j2))
				inc2=inc2+fftlength_x
			enddo

			inc1=inc1+isize2
			
		enddo
	enddo
	
	deallocate(work1_ffts,work2_ffts)
	allocate (work1_ffts(fftlength_x))
    allocate (work2_ffts(fftlength_x))
								
	do j2=0,isize2-fftlength_x,fftlength_x					! x-tranform
		inc1=j2
		do j3=1,fftlength_z
			inc2=inc1
			do j1=1,fftlength_x
				inc2=inc2+1
				cdum=fftout(inc2)
				work1_ffts(j1)=real(cdum)
				work2_ffts(j1)=imag(cdum)
			enddo
			
			call FASTF_S(work1_ffts,work2_ffts,fftlength_x,itype,ifault)
			
			if(ifault.eq.1) then
				write(*,*)'Error in arguments for FASTF_S: j2 =',j2/fftlength_x+1,' j3=',j3
				stop
			endif
			
			inc2=inc1
			do j1=1,fftlength_x
				inc2=inc2+1
				fftout(inc2)=cmplx(work1_ffts(j1),work2_ffts(j1))
			enddo

			inc1=inc1+isize2
			
		enddo
	enddo						
	
	deallocate(work1_ffts,work2_ffts)
		
	return
	end
      
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!  Radix 4 complex FFT
!
!  Usage: CALL FASTF( XREAL, XIMAG, ISIZE, ITYPE, IFAULT )
!
!  Arguments:
!     XREAL - array containing real parts of transform sequence  (i/o)
!     XIMAG - array containing imaginary parts of transform sequence  (i/o)
!     ISIZE - Size of transforms -- ISIZE = 4*2**M  (i)
!     ITYPE -  +1 to denote forward transform
!              -1 to denote backward transform  (i)
!     IFAULT -  1 if error in arguments, 0 otherwise  (o)
!
!  Forward transform computes
!     X(k) = sum_{j=0}^{ISIZE-1} x(j)*exp(-2ijk*pi/ISIZE)
!
!  Backward transform computes 
!     x(j) = (1/ISIZE) * sum_{k=0}^{ISIZE-1} X(k)*exp(2ijk*pi/ISIZE)
!
!  Notice that these transforms are normalized in such a way that a call
!  to the forward transform followed by a call to the backward transform
!  will result in the original sequence.
!
!  This is a modified (slightly) version of the original algorithm
!  referred to below.  I believe that the original author was D. Monro,
!  Imperial College, London.  (I have not checked out the reference, but
!  please let me know if you have.)
!
!  Modifications -
!     Alg. 83.1 -- Entry error checking was rewritten
!     Alg. 83.2 -- Normalization was switched to backward transform
!     Alg. 83.3 -- Unchanged
!
!  Steve Kifowit, kifowit@math.niu.edu, July 1997 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!
      SUBROUTINE FASTF_D( XREAL, XIMAG, ISIZE, ITYPE, IFAULT )
!
!        ALGORITHM AS 83.1  APPL. STATIST. (1975) VOL.24, P.153
!
!        RADIX 4 COMPLEX DISCRETE FAST FOURIER TRANSFORM WITH
!        UNSCRAMBLING OF THE TRANSFORMED ARRAYS
!
      REAL(8) XREAL(*), XIMAG(*)
!
!        CHECK FOR VALID TRANSFORM SIZE - UP TO 2 ** MAX2
!
      DATA MAX2 /20/
!
      IFAULT = 1
      IF ( ISIZE .LT. 4 ) RETURN
      II = 4
      IPOW = 2
 1    IF ( ( II - ISIZE ) .NE. 0 ) THEN
	 II = II * 2
	 IPOW = IPOW + 1
	 IF ( IPOW .GT. MAX2 ) RETURN
	 GOTO 1
      ENDIF
      IF ( IABS( ITYPE ) .NE. 1 ) RETURN
!
!        IF WE REACH THIS POINT, THERE ARE NO ENTRY ERRORS
!
      IFAULT = 0
!
!        CALL FASTG (ALGORITHM AS 83.2) TO PERFORM THE TRANSFORM
!
      CALL FASTG_D( XREAL, XIMAG, ISIZE, ITYPE )
!
!        CALL SCRAM (ALGORITHM AS 83.3)
!        TO UNSCRAMBLE THE RESULTS
!
      CALL SCRAM_D( XREAL, XIMAG, ISIZE, IPOW )
      RETURN
! ... End of subroutine FASTF ...
      END
!
      SUBROUTINE FASTG_D( XREAL, XIMAG, N, ITYPE )
!
!        ALGORITHM AS 83.2  APPL. STATIST. (1975) VOL.24, P.153
!
!        RADIX 4 COMPLEX DISCRETE FAST FOURIER TRANSFORM WITHOUT
!        UNSCRAMBLING, SUITABLE FOR CONVOLUTIONS OR OTHER
!        APPLICATIONS WHICH DO NOT REQUIRE UNSCRAMBLING.
!        SUBROUTINE FASTF USES THIS ROUTINE FOR TRANSFORMATION
!        AND ALSO PROVIDES UNSCRAMBLING
!
      REAL(8) XREAL(*), XIMAG(*), BCOS, BSIN, CW1, CW2, CW3, PI, &
        SW1, SW2, SW3, TEMPR, X1, X2, X3, XS0, XS1, XS2, XS3, &
        Y1, Y2, Y3, YS0, YS1, YS2, YS3, Z, ZERO, HALF, ONE, &
        ONE5, TWO, FOUR, ZATAN, ZFLOAT, ZSIN
!
      DATA ZERO, HALF, ONE, ONE5, TWO, FOUR &
           /0.0,  0.5, 1.0,  1.5, 2.0,  4.0/
!
      ZATAN(Z) = ATAN(Z)
      ZFLOAT(K) = FLOAT(K)
      ZSIN(Z) = SIN(Z)
!
      PI = FOUR * ZATAN(ONE)
      IFACA = N / 4
      IF ( ITYPE .GT. 0 ) GOTO 5
!
!        IF THIS IS TO BE AN INVERSE TRANSFORM, CONJUGATE THE DATA
!
      DO 4, K = 1, N
         XIMAG(K) = -XIMAG(K)
 4    CONTINUE
 5    IFCAB = IFACA * 4
!
!        DO THE TRANSFORMS REQUIRED BY THIS STAGE
!
      Z = PI / ZFLOAT(IFCAB)
      BCOS = -TWO * ZSIN(Z) ** 2
      BSIN = ZSIN(TWO * Z)
      CW1 = ONE
      SW1 = ZERO
      DO 10, LITLA = 1, IFACA
         DO 8, I0 = LITLA, N, IFCAB
!
!        THIS IS THE MAIN CALCULATION OF RADIX 4 TRANSFORMS
!
            I1 = I0 + IFACA
            I2 = I1 + IFACA
            I3 = I2 + IFACA
            XS0 = XREAL(I0) + XREAL(I2)
            XS1 = XREAL(I0) - XREAL(I2)
            YS0 = XIMAG(I0) + XIMAG(I2)
            YS1 = XIMAG(I0) - XIMAG(I2)
            XS2 = XREAL(I1) + XREAL(I3)
            XS3 = XREAL(I1) - XREAL(I3)
            YS2 = XIMAG(I1) + XIMAG(I3)
            YS3 = XIMAG(I1) - XIMAG(I3)
            XREAL(I0) = XS0 + XS2
            XIMAG(I0) = YS0 + YS2
            X1 = XS1 + YS3
            Y1 = YS1 - XS3
            X2 = XS0 - XS2
            Y2 = YS0 - YS2
            X3 = XS1 - YS3
            Y3 = YS1 + XS3
            IF ( LITLA .GT. 1 ) GOTO 7
            XREAL(I2) = X1
            XIMAG(I2) = Y1
            XREAL(I1) = X2
            XIMAG(I1) = Y2
            XREAL(I3) = X3
            XIMAG(I3) = Y3
            GOTO 8
!
!        MULTIPLY BY TWIDDLE FACTORS IF REQUIRED
!
 7          XREAL(I2) = X1 * CW1 + Y1 * SW1
            XIMAG(I2) = Y1 * CW1 - X1 * SW1
            XREAL(I1) = X2 * CW2 + Y2 * SW2
            XIMAG(I1) = Y2 * CW2 - X2 * SW2
            XREAL(I3) = X3 * CW3 + Y3 * SW3
            XIMAG(I3) = Y3 * CW3 - X3 * SW3
 8       CONTINUE
         IF ( LITLA .EQ. IFACA ) GOTO 10
!
!        CALCULATE A NEW SET OF TWIDDLE FACTORS
!
         Z = CW1 * BCOS - SW1 * BSIN + CW1
         SW1 = BCOS * SW1 + BSIN * CW1 + SW1
         TEMPR = ONE5 - HALF * ( Z * Z + SW1 * SW1 )
         CW1 = Z * TEMPR
         SW1 = SW1 * TEMPR
         CW2 = CW1 * CW1 - SW1 * SW1
         SW2 = TWO * CW1 * SW1
         CW3 = CW1 * CW2 - SW1 * SW2
         SW3 = CW1 * SW2 + CW2 * SW1
 10   CONTINUE
      IF ( IFACA .LE. 1 ) GOTO 14
!
!        SET UP THE TRANSFORM SPLIT FOR THE NEXT STAGE
!
      IFACA = IFACA / 4
      IF ( IFACA .GT. 0 ) GOTO 5
!
!        THIS IS THE CALCULATION OF A RADIX TWO STAGE
!
      DO 13, K = 1, N, 2
         TEMPR = XREAL(K) + XREAL(K + 1)
         XREAL(K + 1) = XREAL(K) - XREAL(K + 1)
         XREAL(K) = TEMPR
         TEMPR = XIMAG(K) + XIMAG(K + 1)
         XIMAG(K + 1) = XIMAG(K) - XIMAG(K + 1)
         XIMAG(K) = TEMPR
 13   CONTINUE
 14   IF ( ITYPE .GT. 0 ) GOTO 17
!
!        IF THIS WAS AN INVERSE TRANSFORM, CONJUGATE AND SCALE
!        THE RESULT
!
      Z = ONE / ZFLOAT(N) 
      DO 16, K = 1, N
         XIMAG(K) = -XIMAG(K) * Z
	 XREAL(K) = XREAL(K) * Z
 16   CONTINUE
!
 17   RETURN
! ... End of subroutine FASTG ...
      END
!
      SUBROUTINE SCRAM_D( XREAL, XIMAG, N, IPOW )
!
!        ALGORITHM AS 83.3  APPL. STATIST. (1975) VOL.24, P.153
!
!        SUBROUTINE FOR UNSCRAMBLING FFT DATA.
!
      REAL(8) XREAL(*), XIMAG(*), TEMPR
      INTEGER L(19)
      EQUIVALENCE     (L1,   L(1)), (L2,   L(2)), (L3,   L(3)), &
       (L4,   L(4)), (L5,   L(5)), (L6,   L(6)), (L7,   L(7)), &
       (L8,   L(8)), (L9,   L(9)), (L10, L(10)), (L11, L(11)), &
       (L12, L(12)), (L13, L(13)), (L14, L(14)), (L15, L(15)), &
       (L16, L(16)), (L17, L(17)), (L18, L(18)), (L19, L(19))
!
      II = 1
      ITOP = 2 ** ( IPOW - 1 )
      I = 20 - IPOW
      DO 5, K = 1, I
         L(K) = II
 5    CONTINUE
      L0 = II
      I = I + 1
      DO 6, K = I, 19
         II = II * 2
         L(K) = II
 6    CONTINUE
      II = 0
      DO 9 J1 = 1, L1, L0
       DO 9 J2 = J1, L2, L1
        DO 9 J3 = J2, L3, L2
         DO 9 J4 = J3, L4, L3
          DO 9 J5 = J4, L5, L4
           DO 9 J6 = J5, L6, L5
            DO 9 J7 = J6, L7, L6
             DO 9 J8 = J7, L8, L7
              DO 9 J9 = J8, L9, L8
               DO 9 J10 = J9, L10, L9
                DO 9 J11 = J10, L11, L10
                 DO 9 J12 = J11, L12, L11
                  DO 9 J13 = J12, L13, L12
                   DO 9 J14 = J13, L14, L13
                    DO 9 J15 = J14, L15, L14
                     DO 9 J16 = J15, L16, L15
                      DO 9 J17 = J16, L17, L16
                       DO 9 J18 = J17, L18, L17
                        DO 9 J19 = J18, L19, L18
                         J20 = J19
                         DO 9 I = 1, 2
                            II = II + 1
                            IF (II .GE. J20) GOTO 8
!
!        J20 IS THE BIT-REVERSE OF II
!        PAIRWISE INTERCHANGE
!
                            TEMPR = XREAL(II)
                            XREAL(II) = XREAL(J20)
                            XREAL(J20) = TEMPR
                            TEMPR = XIMAG(II)
                            XIMAG(II) = XIMAG(J20)
                            XIMAG(J20) = TEMPR
 8                          J20 = J20 + ITOP
 9    CONTINUE
      RETURN
! ... End of subroutine SCRAM ...
      END
      
! cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!  Radix 4 complex FFT(for single precision)
!
!  Usage: CALL FASTF4( XREAL, XIMAG, ISIZE, ITYPE, IFAULT )
!
!  Arguments:
!     XREAL - array containing real parts of transform sequence  (i/o)
!     XIMAG - array containing imaginary parts of transform sequence  (i/o)
!     ISIZE - Size of transforms -- ISIZE = 4*2**M  (i)
!     ITYPE -  +1 to denote forward transform
!              -1 to denote backward transform  (i)
!     IFAULT -  1 if error in arguments, 0 otherwise  (o)
!
!  Forward transform computes
!     X(k) = sum_{j=0}^{ISIZE-1} x(j)*exp(-2ijk*pi/ISIZE)
!
!  Backward transform computes 
!     x(j) = (1/ISIZE) * sum_{k=0}^{ISIZE-1} X(k)*exp(2ijk*pi/ISIZE)
!
!  Notice that these transforms are normalized in such a way that a call
!  to the forward transform followed by a call to the backward transform
!  will result in the original sequence.
!
!  This is a modified (slightly) version of the original algorithm
!  referred to below.  I believe that the original author was D. Monro,
!  Imperial College, London.  (I have not checked out the reference, but
!  please let me know if you have.)
!
!  Modifications -
!     Alg. 83.1 -- Entry error checking was rewritten
!     Alg. 83.2 -- Normalization was switched to backward transform
!     Alg. 83.3 -- Unchanged
!
!  Steve Kifowit, kifowit@math.niu.edu, July 1997 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!
      SUBROUTINE FASTF_S( XREAL, XIMAG, ISIZE, ITYPE, IFAULT )
!
!        ALGORITHM AS 83.1  APPL. STATIST. (1975) VOL.24, P.153
!
!        RADIX 4 COMPLEX DISCRETE FAST FOURIER TRANSFORM WITH
!        UNSCRAMBLING OF THE TRANSFORMED ARRAYS
!
      REAL(4) XREAL(*), XIMAG(*)
!
!        CHECK FOR VALID TRANSFORM SIZE - UP TO 2 ** MAX2
!
      DATA MAX2 /20/
!
      IFAULT = 1
      IF ( ISIZE .LT. 4 ) RETURN
      II = 4
      IPOW = 2
 1    IF ( ( II - ISIZE ) .NE. 0 ) THEN
	 II = II * 2
	 IPOW = IPOW + 1
	 IF ( IPOW .GT. MAX2 ) RETURN
	 GOTO 1
      ENDIF
      IF ( IABS( ITYPE ) .NE. 1 ) RETURN
!
!        IF WE REACH THIS POINT, THERE ARE NO ENTRY ERRORS
!
      IFAULT = 0
!
!        CALL FASTG (ALGORITHM AS 83.2) TO PERFORM THE TRANSFORM
!
      CALL FASTG_S( XREAL, XIMAG, ISIZE, ITYPE )
!
!        CALL SCRAM (ALGORITHM AS 83.3)
!        TO UNSCRAMBLE THE RESULTS
!
      CALL SCRAM_S( XREAL, XIMAG, ISIZE, IPOW )
      RETURN
! ... End of subroutine FASTF ...
      END
!
      SUBROUTINE FASTG_S( XREAL, XIMAG, N, ITYPE )
!
!        ALGORITHM AS 83.2  APPL. STATIST. (1975) VOL.24, P.153
!
!        RADIX 4 COMPLEX DISCRETE FAST FOURIER TRANSFORM WITHOUT
!        UNSCRAMBLING, SUITABLE FOR CONVOLUTIONS OR OTHER
!        APPLICATIONS WHICH DO NOT REQUIRE UNSCRAMBLING.
!        SUBROUTINE FASTF USES THIS ROUTINE FOR TRANSFORMATION
!        AND ALSO PROVIDES UNSCRAMBLING
!
      REAL(4) XREAL(*), XIMAG(*), BCOS, BSIN, CW1, CW2, CW3, PI, &
        SW1, SW2, SW3, TEMPR, X1, X2, X3, XS0, XS1, XS2, XS3, &
        Y1, Y2, Y3, YS0, YS1, YS2, YS3, Z, ZERO, HALF, ONE, &
        ONE5, TWO, FOUR, ZATAN, ZFLOAT, ZSIN
!
      DATA ZERO, HALF, ONE, ONE5, TWO, FOUR &
           /0.0,  0.5, 1.0,  1.5, 2.0,  4.0/
!
      ZATAN(Z) = ATAN(Z)
      ZFLOAT(K) = FLOAT(K)
      ZSIN(Z) = SIN(Z)
!
      PI = FOUR * ZATAN(ONE)
      IFACA = N / 4
      IF ( ITYPE .GT. 0 ) GOTO 5
!
!        IF THIS IS TO BE AN INVERSE TRANSFORM, CONJUGATE THE DATA
!
      DO 4, K = 1, N
         XIMAG(K) = -XIMAG(K)
 4    CONTINUE
 5    IFCAB = IFACA * 4
!
!        DO THE TRANSFORMS REQUIRED BY THIS STAGE
!
      Z = PI / ZFLOAT(IFCAB)
      BCOS = -TWO * ZSIN(Z) ** 2
      BSIN = ZSIN(TWO * Z)
      CW1 = ONE
      SW1 = ZERO
      DO 10, LITLA = 1, IFACA
         DO 8, I0 = LITLA, N, IFCAB
!
!        THIS IS THE MAIN CALCULATION OF RADIX 4 TRANSFORMS
!
            I1 = I0 + IFACA
            I2 = I1 + IFACA
            I3 = I2 + IFACA
            XS0 = XREAL(I0) + XREAL(I2)
            XS1 = XREAL(I0) - XREAL(I2)
            YS0 = XIMAG(I0) + XIMAG(I2)
            YS1 = XIMAG(I0) - XIMAG(I2)
            XS2 = XREAL(I1) + XREAL(I3)
            XS3 = XREAL(I1) - XREAL(I3)
            YS2 = XIMAG(I1) + XIMAG(I3)
            YS3 = XIMAG(I1) - XIMAG(I3)
            XREAL(I0) = XS0 + XS2
            XIMAG(I0) = YS0 + YS2
            X1 = XS1 + YS3
            Y1 = YS1 - XS3
            X2 = XS0 - XS2
            Y2 = YS0 - YS2
            X3 = XS1 - YS3
            Y3 = YS1 + XS3
            IF ( LITLA .GT. 1 ) GOTO 7
            XREAL(I2) = X1
            XIMAG(I2) = Y1
            XREAL(I1) = X2
            XIMAG(I1) = Y2
            XREAL(I3) = X3
            XIMAG(I3) = Y3
            GOTO 8
!
!        MULTIPLY BY TWIDDLE FACTORS IF REQUIRED
!
 7          XREAL(I2) = X1 * CW1 + Y1 * SW1
            XIMAG(I2) = Y1 * CW1 - X1 * SW1
            XREAL(I1) = X2 * CW2 + Y2 * SW2
            XIMAG(I1) = Y2 * CW2 - X2 * SW2
            XREAL(I3) = X3 * CW3 + Y3 * SW3
            XIMAG(I3) = Y3 * CW3 - X3 * SW3
 8       CONTINUE
         IF ( LITLA .EQ. IFACA ) GOTO 10
!
!        CALCULATE A NEW SET OF TWIDDLE FACTORS
!
         Z = CW1 * BCOS - SW1 * BSIN + CW1
         SW1 = BCOS * SW1 + BSIN * CW1 + SW1
         TEMPR = ONE5 - HALF * ( Z * Z + SW1 * SW1 )
         CW1 = Z * TEMPR
         SW1 = SW1 * TEMPR
         CW2 = CW1 * CW1 - SW1 * SW1
         SW2 = TWO * CW1 * SW1
         CW3 = CW1 * CW2 - SW1 * SW2
         SW3 = CW1 * SW2 + CW2 * SW1
 10   CONTINUE
      IF ( IFACA .LE. 1 ) GOTO 14
!
!        SET UP THE TRANSFORM SPLIT FOR THE NEXT STAGE
!
      IFACA = IFACA / 4
      IF ( IFACA .GT. 0 ) GOTO 5
!
!        THIS IS THE CALCULATION OF A RADIX TWO STAGE
!
      DO 13, K = 1, N, 2
         TEMPR = XREAL(K) + XREAL(K + 1)
         XREAL(K + 1) = XREAL(K) - XREAL(K + 1)
         XREAL(K) = TEMPR
         TEMPR = XIMAG(K) + XIMAG(K + 1)
         XIMAG(K + 1) = XIMAG(K) - XIMAG(K + 1)
         XIMAG(K) = TEMPR
 13   CONTINUE
 14   IF ( ITYPE .GT. 0 ) GOTO 17
!
!        IF THIS WAS AN INVERSE TRANSFORM, CONJUGATE AND SCALE
!        THE RESULT
!
      Z = ONE / ZFLOAT(N) 
      DO 16, K = 1, N
         XIMAG(K) = -XIMAG(K) * Z
	 XREAL(K) = XREAL(K) * Z
 16   CONTINUE
!
 17   RETURN
! ... End of subroutine FASTG ...
      END
!
      SUBROUTINE SCRAM_S( XREAL, XIMAG, N, IPOW )
!
!        ALGORITHM AS 83.3  APPL. STATIST. (1975) VOL.24, P.153
!
!        SUBROUTINE FOR UNSCRAMBLING FFT DATA.
!
      REAL(4) XREAL(*), XIMAG(*), TEMPR
      INTEGER L(19)
      EQUIVALENCE     (L1,   L(1)), (L2,   L(2)), (L3,   L(3)), &
       (L4,   L(4)), (L5,   L(5)), (L6,   L(6)), (L7,   L(7)), &
       (L8,   L(8)), (L9,   L(9)), (L10, L(10)), (L11, L(11)), &
       (L12, L(12)), (L13, L(13)), (L14, L(14)), (L15, L(15)), &
       (L16, L(16)), (L17, L(17)), (L18, L(18)), (L19, L(19))
!
      II = 1
      ITOP = 2 ** ( IPOW - 1 )
      I = 20 - IPOW
      DO 5, K = 1, I
         L(K) = II
 5    CONTINUE
      L0 = II
      I = I + 1
      DO 6, K = I, 19
         II = II * 2
         L(K) = II
 6    CONTINUE
      II = 0
      DO 9 J1 = 1, L1, L0
       DO 9 J2 = J1, L2, L1
        DO 9 J3 = J2, L3, L2
         DO 9 J4 = J3, L4, L3
          DO 9 J5 = J4, L5, L4
           DO 9 J6 = J5, L6, L5
            DO 9 J7 = J6, L7, L6
             DO 9 J8 = J7, L8, L7
              DO 9 J9 = J8, L9, L8
               DO 9 J10 = J9, L10, L9
                DO 9 J11 = J10, L11, L10
                 DO 9 J12 = J11, L12, L11
                  DO 9 J13 = J12, L13, L12
                   DO 9 J14 = J13, L14, L13
                    DO 9 J15 = J14, L15, L14
                     DO 9 J16 = J15, L16, L15
                      DO 9 J17 = J16, L17, L16
                       DO 9 J18 = J17, L18, L17
                        DO 9 J19 = J18, L19, L18
                         J20 = J19
                         DO 9 I = 1, 2
                            II = II + 1
                            IF (II .GE. J20) GOTO 8
!
!        J20 IS THE BIT-REVERSE OF II
!        PAIRWISE INTERCHANGE
!
                            TEMPR = XREAL(II)
                            XREAL(II) = XREAL(J20)
                            XREAL(J20) = TEMPR
                            TEMPR = XIMAG(II)
                            XIMAG(II) = XIMAG(J20)
                            XIMAG(J20) = TEMPR
 8                          J20 = J20 + ITOP
 9    CONTINUE
      RETURN
! ... End of subroutine SCRAM ...
      END



    
    
    
    
    
    
    
        
