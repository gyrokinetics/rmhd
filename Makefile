EXECUTABLE	:= rmhd
CUFILES         := DevFunc_MHDkernel.cu
CCFILES         := c_FGPU.cpp
CCFILES         += c_DevFunc_v5.cpp
CFILES          := fortran.c

F90FLAGS = -assume nounderscore
FLIBS = 
ifeq ($(debug),on)
F90FLAGS += -g -implicitnone -warn all -check bounds -traceback 
else
F90FLAGS += -O3 
endif

F90FILES := rmhd.f90 mhd_data.f90 devObject.f90 fields.f90 \
	grid.f90 constants.f90 FGPU.f90 \
	mhd_device_func.f90 nlps.f90 FFTRadix4.f90 fft_work.f90 

include common.mk

$(OBJDIR)/rmhd.f90_o: $(OBJDIR)/mhd_data.f90_o \
	$(OBJDIR)/fields.f90_o $(OBJDIR)/linear.f90_o $(OBJDIR)/nlps.f90_o \
	$(OBJDIR)/grid.f90_o $(OBJDIR)/constants.f90_o \
	$(OBJDIR)/devObject.f90_o $(OBJDIR)/mhd_device_func.f90_o 
$(OBJDIR)/mhd_data.f90_o: $(OBJDIR)/grid.f90_o
$(OBJDIR)/fields.f90_o: $(OBJDIR)/devObject.f90_o $(OBJDIR)/grid.f90_o
$(OBJDIR)/linear.f90_o: $(OBJDIR)/devObject.f90_o $(OBJDIR)/mhd_data.f90_o $(OBJDIR)/grid.f90_o 
$(OBJDIR)/nlps.f90_o: $(OBJDIR)/devObject.f90_o $(OBJDIR)/constants.f90_o \
	$(OBJDIR)/grid.f90_o $(OBJDIR)/mhd_device_func.f90_o \
	$(OBJDIR)/FFTRadix4.f90_o $(OBJDIR)/fft_work.f90_o 
$(OBJDIR)/gputest.f90_o: $(OBJDIR)/devObject.f90_o 
$(OBJDIR)/mhd_device_func.f90_o: $(OBJDIR)/devObject.f90_o
$(OBJDIR)/devObject.f90_o: $(OBJDIR)/fortran.c_o $(OBJDIR)/FGPU.f90_o
$(OBJDIR)/FFTRadix4.f90_o: $(OBJDIR)/fft_work.f90_o
$(OBJDIR)/grid.f90_o: $(OBJDIR)/constants.f90_o
$(OBJDIR)/rmhd.f90_o: $(OBJDIR)/DevFunc_MHDkernel.cu_o





